overview: intro do-overview
intro: do-show-intro

init: do-install-git-hooks do-copy-env update

start: intro do-start do-show-url
stop: intro do-stop
restart: intro do-restart
shell: intro do-shell
logs: intro do-logs
test: intro do-tests
fix: intro do-fixes
watch-ts: intro do-typescript-watch

update: intro do-update

do-overview:
	@echo "\nThe following commands are available\n"
	@echo "    make init             Initialize the project"
	@echo "    make start            Start the app container"
	@echo "    make stop             Stop and remove containers / networks"
	@echo "    make restart          Restart the app container"
	@echo "    make update           Update dependencies"
	@echo "    make shell            Open shell in app container"
	@echo "    make logs             Show logs of the app container"
	@echo "    make test             Run all tests"
	@echo "    make fix              Do automatic codestyle fixes"
	@echo "    make watch-ts         Watch for typescript compilation errors"


# Snippets

run-app = docker-compose run --rm app

# General commands

do-show-intro:
	@echo "---- PROJECT ----"

do-show-url:
	@echo "> The app is running on http://localhost:2000"

do-install-git-hooks:
	@echo "> Installing git hooks"
	cp dev/git-hooks/* .git/hooks
	chmod +x .git/hooks/*

do-copy-env:
	@echo "> Copy .env.local"
	cp -n .env.local.dist .env.local

# Container commands

do-start:
	@echo "> Starting"
	docker-compose up -d

do-stop:
	@echo "> Stopping"
	docker-compose down

do-restart:
	@echo "> Restarting"
	docker-compose restart

do-shell:
	@echo "> Open shell in app Container"
	${run-app} sh

do-update:
	@echo "> Updating npm packages"
	${run-app} npm install

do-logs:
	@echo "> Showing logs"
	docker-compose logs -f app

# tests

do-tests: \
	do-unit-test \
	do-typescript-check \
	do-eslint-check \
	do-stylelint-check

do-unit-test:
	@echo "> Unit test"
	${run-app} npm run unit-test

do-typescript-check:
	@echo "> Typescript check"
	${run-app} npm run typescript

do-eslint-check:
	@echo "> ESLint check"
	${run-app} npm run eslint

do-stylelint-check:
	@echo "> Stylelint check"
	${run-app} npm run stylelint

do-typescript-watch:
	@echo "> Typescript check"
	${run-app} npm run typescript-watch

# code fixes

do-fixes: \
	do-eslint-fix \
	do-stylelint-fix

do-eslint-fix:
	@echo "> ESLint check"
	${run-app} npm run eslint-fix

do-stylelint-fix:
	@echo "> Stylelint check"
	${run-app} npm run stylelint-fix
