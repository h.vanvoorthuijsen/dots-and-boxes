const path = require('path');

const CWD = process.cwd();

module.exports = {
  data: "@import 'variables'; @import 'variables';",
  includePaths: [
    path.resolve(CWD, 'node_modules'),
    path.resolve(CWD, 'src/scss'),
  ],
};
