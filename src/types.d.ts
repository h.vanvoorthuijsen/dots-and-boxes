type MoveType = {
  playerId: UserIdType;
  x: number;
  y: number;
  direction: 'horizontal' | 'vertical';
  playerConquered?: boolean;
};

type GameType = {
  width: number;
  height: number;
  players: UserIdType[];
  currentPlayerId: UserIdType;
  score: number[];
};

type UserType = {
  name: string;
  color: string;
};

type UserIdType = string;

type GameConnectionType = GameConnectionOfferType | GameConnectionAnswerType;

type GameConnectionOfferType = {
  offer: SignalData;
  offerCreatedOn: firebase.firestore.Timestamp;
};

type GameConnectionAnswerType = {
  answer: SignalData;
  answerCreatedOn: firebase.firestore.Timestamp;
};

type SignalData = import('simple-peer').SignalData;

type GameIdType = string;
