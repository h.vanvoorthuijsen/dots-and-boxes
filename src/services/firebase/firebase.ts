import firebase from 'firebase/app';
import 'firebase/firestore';

import firebaseConfig from './config';

firebase.initializeApp(firebaseConfig);

export const firestore = firebase.firestore();
export const timeStamp = firebase.firestore.Timestamp;
