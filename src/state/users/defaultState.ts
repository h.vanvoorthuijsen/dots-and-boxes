const defaultUsers: {
  users: Map<UserIdType, UserType>;
  currentUserId?: UserIdType;
} = {
  users: new Map(),
};

export default defaultUsers;
