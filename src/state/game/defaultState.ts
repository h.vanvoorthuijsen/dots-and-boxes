const defaultGame: GameType = {
  width: 4,
  height: 4,
  players: [],
  currentPlayerId: '',
  score: [],
};

export default defaultGame;
