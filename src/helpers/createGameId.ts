import { customAlphabet } from 'nanoid';
export const characters = '2346789ABCDEFGHJKLMNPQRTUVWXYZabcdefghijkmnpqrtwxyz';

const createGameId = (nrOfCharacters = 5) =>
  customAlphabet(characters, nrOfCharacters)();

export default createGameId;
