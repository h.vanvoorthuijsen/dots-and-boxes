import React from 'react';
import bemClassNames from '@enrise/bemclassnames';
import './Dot.scss';

const cn = bemClassNames('Dot');

const Dot = () => <div className={cn()}></div>;

export default Dot;
