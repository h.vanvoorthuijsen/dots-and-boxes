type BoxStateType = {
  top: boolean;
  bottom: boolean;
  left: boolean;
  right: boolean;
  lastPlayer?: UserIdType;
};
